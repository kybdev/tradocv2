<?php

namespace Modules\Frontend\Controllers;

use \Phalcon\Mvc\View;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    // public function onConstruct()
    // {
    //     /**
    //      *  Load the Navigation Menu.
    //      */
    //     $menu                      = $this->curl("/menu/viewfrontendmenu/[mainmenu]");
    //     $this->view->MENU_CONTENTS = $menu->data->data;
    //     $this->view->children      = $menu->data->children;
    //
    //     $data                 = $this->curl("/settings/siteinfo");
    //     $this->view->siteinfo = $data->data->siteinfo;
    //
    //     $googleScript                = $this->curl("/settings/gscript");
    //     $this->view->googleAnalytics = $googleScript->data->gscript;
    //
    //     $socialmedia             = $this->curl("/settings/socialmedia/frontend");
    //     $this->view->socialmedia = $socialmedia->data;
    //     $this->view->maintenance = $this->curl("/settings/maintenance")->data->value1;
    //     $this->view->scriptLocation  = $this->config->application->scriptLocation;
    // }

    protected function initialize()
    {
        /* Generate a config.js */
        // $this->createJsConfig();
        // 
        // $this->view->base_url = $this->config->application->BaseURL;
        // $this->view->api_url  = $this->config->application->ApiURL;
        // $this->view->scriptLocation  = $this->config->application->scriptLocation;
    }

    public function loaders($loaders)
    {
        if ($loaders['css']) {
            $this->cssLoader($loaders['css']);
        }

        if ($loaders['vendors']) {
            $this->jsBeforeAngularApp($loaders['vendors']);
        }

        if ($loaders['appjs']) {
            $this->angularLoader($loaders['appjs']);
        }
    }

    public function jsBeforeAngularApp($js)
    {
        $modules = array();
        $scripts = '';
        foreach ($js as $key => $val) {
            $scripts .= $this->tag->javascriptInclude($val);
            $modules[] = $key;
        }
        $this->view->modules = (!empty($modules) ? $modules : array());
        $this->view->jsBeforeAngularApp = $scripts;
    }

    public function angularLoader($ang)
    {
        $modules = array();
        $scripts = '';
        foreach ($ang as $key => $val) {
            $scripts .= $this->tag->javascriptInclude($val);
            $modules[] = $key;
        }
        $this->view->modules         = (!empty($modules) ? $modules : array());
        $this->view->otherjavascript = $scripts;
    }

    public function cssLoader($css)
    {
        $stylesheets = '';
        foreach ($css as $key => $val) {
            $stylesheets .= $this->tag->stylesheetLink($val);
        }
        $this->view->othercss = $stylesheets;
    }

    public function httpPost($url, $params)
    {
        $postData = '';
        //create name value pairs seperated by &
        foreach ($params as $k => $v) {
            $postData .= $k . '=' . $v . '&';
        }
        rtrim($postData, '&');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $output = curl_exec($ch);

        curl_close($ch);
        return $output;
    }

    public function curl($url)
    {
        $service_url = $this->config->application->ApiURL . '/' . $url;
        $curl        = curl_init($service_url);
        //curl_setopt($curl, CURLOPT_CAINFO, $this->config->application->curlRest);
        // curl_setopt($curl, CURLOPT_CAINFO);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_dump($info));
        }
        curl_close($curl);
        return $decoded = json_decode($curl_response);
    }

    private function createJsConfig()
    {
        $script = "app.constant('Config', {
            // This is a generated Config
            // Any changes you make in this file will be replaced
            // Place your changes in /dist/config/config.yours.php file \n";
        foreach ($this->config->application as $key => $val) {
            $script .= $key . ' : "' . $val . '",' . "\n";
        }
        $script .= "});";
        $fileName = "../public/fe/scripts/config.js";

        $exist = file_get_contents($fileName);

        if ($exist != $script) {

            unlink(__DIR__ . '/../../' . $fileName);

            file_put_contents($fileName, $script);

        }
    }

    public function awsmediaurl($media)
    {
        return $this->config->application->amazonlink . '/mastersite/uploads/media/' . $media['archive'] . $media['reference'];
    }

    public function scriptLocation(){
        return $this->config->application->scriptLocation;
    }
}
