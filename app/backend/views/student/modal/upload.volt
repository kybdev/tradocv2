<div class="modal-header">
	<span class="btn btn-primary btn-file"> Select an image file
		<input type="file" id="fileInput test" file-reader="myupload" />
	</span>
</div>
<form name="Form" id="form1" novalidate ng-submit="addData(Form, myModel)" >
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<div class="cropBox">
					<div class="cropArea">
						<img-crop image="myupload" result-image="myCroppedImage" area-type="{{'square'}}" result-image-size="500"></img-crop>
					</div>
				</div>
			</div>


			<div class="col-md-12">
				<div class="thumbnail" style="border:none !important">
				    <img src="{[{myCroppedImage}]}" alt="">
				</div>
			</div>
		</div>

	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-primary"  ng-click="setImage()">OK</button>
		<button class="btn btn-primary btn-o" ng-click="cancel()">Cancel</button>
	</div>
</form>
