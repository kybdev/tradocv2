<div class="row">
    <h3 class="over-title margin-bottom-15 text-center">Description <span class="text-bold"></span></h3>
    <div class="col-md-6">
        <div class="form-group" ng-class="{'has-error':Form.sex.$dirty && Form.sex.$invalid, 'has-success':Form.sex.$valid}">
            <label class="control-label">
                Sex<span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Present Home Address" class="form-control" name="sex" ng-model="myModel.sex" required />
            <span class="error text-small block" ng-if="Form.sex.$dirty && Form.sex.$invalid">Sexis required</span>
            <span class="success text-small" ng-if="Form.sex.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.age.$dirty && Form.age.$invalid, 'has-success':Form.age.$valid}">
            <label class="control-label">
                Age <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter age" class="form-control" name="age" ng-model="myModel.age" required />
            <span class="error text-small block" ng-if="Form.age.$dirty && Form.age.$invalid">Age is required</span>
            <span class="success text-small" ng-if="Form.age.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.height.$dirty && Form.height.$invalid, 'has-success':Form.height.$valid}">
            <label class="control-label">
                Height (meter) <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Height (meter)" class="form-control" name="height" ng-model="myModel.height" required />
            <span class="error text-small block" ng-if="Form.height.$dirty && Form.height.$invalid">Height (meter) is required</span>
            <span class="success text-small" ng-if="Form.height.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.weight.$dirty && Form.weight.$invalid, 'has-success':Form.weight.$valid}">
            <label class="control-label">
                Weight (Kg) <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Weight (Kg))" class="form-control" name="weight" ng-model="myModel.weight" required />
            <span class="error text-small block" ng-if="Form.weight.$dirty && Form.weight.$invalid">Weight (Kg) is required</span>
            <span class="success text-small" ng-if="Form.weight.$valid">Thank You!</span>
        </div>




    </div>

    <div class="col-md-6">

        <div class="form-group" ng-class="{'has-error':Form.built.$dirty && Form.built.$invalid, 'has-success':Form.built.$valid}">
            <label class="control-label">
                Built: (Heavy, Medium, Light) <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Built: (Heavy, Medium, Light)" class="form-control" name="Built" ng-model="myModel.built" required />
            <span class="error text-small block" ng-if="Form.built.$dirty && Form.built.$invalid">Built: (Heavy, Medium, Light) is required</span>
            <span class="success text-small" ng-if="Form.built.$valid">Thank You!</span>
        </div>


        <div class="form-group" ng-class="{'has-error':Form.complexion.$dirty && Form.complexion.$invalid, 'has-success':Form.complexion.$valid}">
            <label class="control-label">
                Complexion: (Dark, Fair, Light) <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Complexion: (Dark, Fair, Light)" class="form-control" name="complexion" ng-model="myModel.complexion" required />
            <span class="error text-small block" ng-if="Form.complexion.$dirty && Form.complexion.$invalid">Complexion: (Dark, Fair, Light) is required</span>
            <span class="success text-small" ng-if="Form.complexion.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.eyeColor.$dirty && Form.eyeColor.$invalid, 'has-success':Form.eyeColor.$valid}">
            <label class="control-label">
                Color of eyes: <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Color of eyes: " class="form-control" name="eyeColor" ng-model="myModel.eyeColor" required />
            <span class="error text-small block" ng-if="Form.eyeColor.$dirty && Form.eyeColor.$invalid">Color of eyes: is required</span>
            <span class="success text-small" ng-if="Form.eyeColor.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.hairColor.$dirty && Form.hairColor.$invalid, 'has-success':Form.hairColor.$valid}">
            <label class="control-label">
                Color of Hair <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Color of Hair" class="form-control" name="hairColor" ng-model="myModel.hairColor" required />
            <span class="error text-small block" ng-if="Form.hairColor.$dirty && Form.hairColor.$invalid">Color of Hair is required</span>
            <span class="success text-small" ng-if="Form.hairColor.$valid">Thank You!</span>
        </div>

    </div>

    <div class="col-md-12">
        <div class="form-group" ng-class="{'has-error':Form.scarMark.$dirty && Form.scarMark.$invalid, 'has-success':Form.scarMark.$valid}">
            <label class="control-label">
                Scar or Marks & Other distinguishing Feature: <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Scar or Marks & Other distinguishing Feature:" class="form-control" name="scarMark" ng-model="myModel.scarMark" required />
            <span class="error text-small block" ng-if="Form.scarMark.$dirty && Form.scarMark.$invalid">Scar or Marks & Other distinguishing Feature is required</span>
            <span class="success text-small" ng-if="Form.scarMark.$valid">Thank You!</span>
        </div>
    </div>



    <div class="col-md-12">

        <h3 class="over-title margin-bottom-15 text-center">Physical Condition <span class="text-bold"></span></h3>

        <div class="form-group" ng-class="{'has-error':Form.stateHealth.$dirty && Form.stateHealth.$invalid, 'has-success':Form.stateHealth.$valid}">
            <label class="control-label">
                Present state of health (Excellent, Good, Poor) <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Present state of health (Excellent, Good, Poor)" class="form-control" name="stateHealth" ng-model="myModel.stateHealth" required />
            <span class="error text-small block" ng-if="Form.stateHealth.$dirty && Form.stateHealth.$invalid">Present state of health (Excellent, Good, Poor) is required</span>
            <span class="success text-small" ng-if="Form.stateHealth.$valid">Thank You!</span>
        </div>


        <div class="form-group" ng-class="{'has-error':Form.phyMenDef.$dirty && Form.phyMenDef.$invalid, 'has-success':Form.phyMenDef.$valid}">
            <label class="control-label">
                Physical or Mental Defects: <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Physical or Mental Defects" class="form-control" name="phyMenDef" ng-model="myModel.phyMenDef" required />
            <span class="error text-small block" ng-if="Form.phyMenDef.$dirty && Form.phyMenDef.$invalid">Physical or Mental Defects is required</span>
            <span class="success text-small" ng-if="Form.phyMenDef.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.recSerIll.$dirty && Form.recSerIll.$invalid, 'has-success':Form.recSerIll.$valid}">
            <label class="control-label">
                Recent Serious Illness: <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Recent Serious Illness " class="form-control" name="recSerIll" ng-model="myModel.recSerIll" required />
            <span class="error text-small block" ng-if="Form.recSerIll.$dirty && Form.recSerIll.$invalid">Recent Serious Illness is required</span>
            <span class="success text-small" ng-if="Form.recSerIll.$valid">Thank You!</span>
        </div>

    </div>


</div>
