<fieldset>
    <legend><h5><b>ELEMENTARY</b></h5>	</legend>
    <button type="button" class="btn btn-primary btn-xs pull-right" ng-click="form.modal('', 'educBackground.html', 'elementary', myModel )">
        <i class="glyphicon glyphicon-plus"></i> ADD
    </button>
    <h5 class="over-title margin-bottom-15"> <span class="text-bold"></span></h5>
    <table class="table table-hover" id="sample-table-1">
        <thead>
            <tr>
                <th>NAME OF SCHOOL</th>
                <th>LOCATION </th>
                <th>DATE OF ATTENDANCE</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="list in myModel.elementary">
                <td class=""><span ng-bind-html="list.schoolName"></span></td>
                <td class=""><span ng-bind-html="list.location"></span></td>
                <td class=""><span ng-bind-html="list.dateOfAttendance"></span></td>
                <td class="center">
                    <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-transparent btn-xs" tooltip-placement="top" uib-tooltip="Edit" ng-click="form.modalEdit('', 'educBackground.html', list)"><i class="fa fa-pencil"></i></a>
                        <a href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" uib-tooltip="Remove" ng-click="form.removeList($index, myModel.elementary)"><i class="fa fa-times fa fa-white"></i></a>
                    </div>
                    <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group" uib-dropdown>
                            <button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" uib-dropdown-toggle>
                                <i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right dropdown-light" role="menu">
                                <li>
                                    <a href="#">
                                        Edit
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Remove
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>


<fieldset>
    <legend><h5><b>HIGH SCHOOL</b></h5>	</legend>
    <button type="button" class="btn btn-primary btn-xs pull-right" ng-click="form.modal('', 'educBackground.html', 'highSchool', myModel )">
        <i class="glyphicon glyphicon-plus"></i> ADD
    </button>
    <h5 class="over-title margin-bottom-15"> <span class="text-bold"></span></h5>
    <table class="table table-hover" id="sample-table-1">
        <thead>
            <tr>
                <th>NAME OF SCHOOL</th>
                <th>LOCATION </th>
                <th>DATE OF ATTENDANCE</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="list in myModel.highSchool">
                <td class=""><span ng-bind-html="list.schoolName"></span></td>
                <td class=""><span ng-bind-html="list.location"></span></td>
                <td class=""><span ng-bind-html="list.dateOfAttendance"></span></td>
                <td class="center">
                    <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-transparent btn-xs" tooltip-placement="top" uib-tooltip="Edit" ng-click="form.modalEdit('', 'educBackground.html', list)"><i class="fa fa-pencil"></i></a>
                        <a href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" uib-tooltip="Remove" ng-click="form.removeList($index, myModel.highSchool)"><i class="fa fa-times fa fa-white"></i></a>
                    </div>
                    <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group" uib-dropdown>
                            <button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" uib-dropdown-toggle>
                                <i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right dropdown-light" role="menu">
                                <li>
                                    <a href="#">
                                        Edit
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Remove
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend><h5><b>COLLEGE</b></h5>	</legend>
    <button type="button" class="btn btn-primary btn-xs pull-right" ng-click="form.modal('', 'educBackground.html', 'college', myModel )">
        <i class="glyphicon glyphicon-plus"></i> ADD
    </button>
    <h5 class="over-title margin-bottom-15"> <span class="text-bold"></span></h5>
    <table class="table table-hover" id="sample-table-1">
        <thead>
            <tr>
                <th>NAME OF SCHOOL</th>
                <th>LOCATION </th>
                <th>DATE OF ATTENDANCE</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="list in myModel.college">
                <td class=""><span ng-bind-html="list.schoolName"></span></td>
                <td class=""><span ng-bind-html="list.location"></span></td>
                <td class=""><span ng-bind-html="list.dateOfAttendance"></span></td>
                <td class="center">
                    <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-transparent btn-xs" tooltip-placement="top" uib-tooltip="Edit" ng-click="form.modalEdit('', 'educBackground.html', list)"><i class="fa fa-pencil"></i></a>
                        <a href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" uib-tooltip="Remove" ng-click="form.removeList($index, myModel.college)"><i class="fa fa-times fa fa-white"></i></a>
                    </div>
                    <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group" uib-dropdown>
                            <button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" uib-dropdown-toggle>
                                <i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right dropdown-light" role="menu">
                                <li>
                                    <a href="#">
                                        Edit
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Remove
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>


<fieldset>
    <legend><h5><b>POST GRADUATE</b></h5>	</legend>
    <button type="button" class="btn btn-primary btn-xs pull-right" ng-click="form.modal('', 'educBackground.html', 'postGraduate', myModel )">
        <i class="glyphicon glyphicon-plus"></i> ADD
    </button>
    <h5 class="over-title margin-bottom-15"> <span class="text-bold"></span></h5>
    <table class="table table-hover" id="sample-table-1">
        <thead>
            <tr>
                <th>NAME OF SCHOOL</th>
                <th>LOCATION </th>
                <th>DATE OF ATTENDANCE</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="list in myModel.postGraduate">
                <td class=""><span ng-bind-html="list.schoolName"></span></td>
                <td class=""><span ng-bind-html="list.location"></span></td>
                <td class=""><span ng-bind-html="list.dateOfAttendance"></span></td>
                <td class="center">
                    <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-transparent btn-xs" tooltip-placement="top" uib-tooltip="Edit" ng-click="form.modalEdit('', 'educBackground.html', list)"><i class="fa fa-pencil"></i></a>
                        <a href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" uib-tooltip="Remove" ng-click="form.removeList($index, myModel.postGraduate)"><i class="fa fa-times fa fa-white"></i></a>
                    </div>
                    <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group" uib-dropdown>
                            <button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" uib-dropdown-toggle>
                                <i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right dropdown-light" role="menu">
                                <li>
                                    <a href="#">
                                        Edit
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Remove
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend><h5><b>OTHER SCHOOLS ATTENDED</b></h5>	</legend>
    <button type="button" class="btn btn-primary btn-xs pull-right" ng-click="form.modal('', 'educBackground.html', 'otherSchoolAttended', myModel )">
        <i class="glyphicon glyphicon-plus"></i> ADD
    </button>
    <h5 class="over-title margin-bottom-15"> <span class="text-bold"></span></h5>
    <table class="table table-hover" id="sample-table-1">
        <thead>
            <tr>
                <th>NAME OF SCHOOL</th>
                <th>LOCATION </th>
                <th>DATE OF ATTENDANCE</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="list in myModel.otherSchoolAttended">
                <td class=""><span ng-bind-html="list.schoolName"></span></td>
                <td class=""><span ng-bind-html="list.location"></span></td>
                <td class=""><span ng-bind-html="list.dateOfAttendance"></span></td>
                <td class="center">
                    <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-transparent btn-xs" tooltip-placement="top" uib-tooltip="Edit" ng-click="form.modalEdit('', 'educBackground.html', list)"><i class="fa fa-pencil"></i></a>
                        <a href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" uib-tooltip="Remove" ng-click="form.removeList($index, myModel.otherSchoolAttended)"><i class="fa fa-times fa fa-white"></i></a>
                    </div>
                    <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group" uib-dropdown>
                            <button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" uib-dropdown-toggle>
                                <i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right dropdown-light" role="menu">
                                <li>
                                    <a href="#">
                                        Edit
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Remove
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>


<div class="col-md-12">
    <div class="form-group" ng-class="{'has-error':Form.civilSrvc.$dirty && Form.civilSrvc.$invalid, 'has-success':Form.civilSrvc.$valid}">
        <label class="control-label">
            Civil Service Eligibility, If any and other similar qualifications acquired <span class="symbol required"></span>
        </label>
        <input type="text" placeholder="Enter Civil Service Eligibility, If any and other similar qualifications acquired " class="form-control" name="civilSrvc" ng-model="myModel.civilSrvc" required />
        <span class="error text-small block" ng-if="Form.civilSrvc.$dirty && Form.civilSrvc.$invalid">Recent Serious Illness is required</span>
        <span class="success text-small" ng-if="Form.civilSrvc.$valid">Thank You!</span>
    </div>

</div>
